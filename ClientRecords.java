import java.util.Map;
import java.util.HashMap;
import java.net.*;
import java.util.Set;

class ClientRecords {
	private final Map<String, Socket> clients;

	public ClientRecords() {
		this.clients = new HashMap<>();
	}

	public void addClient(String username, Socket clientSocket) {
		this.clients.put(username, clientSocket);
	}

	public void printHashMap() {
		// System.out.println("InsidePrinting");
		for(String clientUsername: this.clients.keySet()) {
			System.out.println(clientUsername);
		}
	}

	public Map<String, Socket> getClients() {
		return new HashMap<>(clients);
	}

	public Set<String> getUsernames() {
		return clients.keySet();
	}

	public void removeClient(String username) {
		clients.remove(username);
	}
}