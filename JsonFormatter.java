class JsonFormatter {
	public String formatInput(String input, String myUsername) {
		// YOUR MESSAGE SHOULD BE LIKE: USERNAME "MSG" MSG
		String to = input.substring(0, input.indexOf("msg") - 1);
		String from = myUsername;
		String msg = input.substring(input.indexOf(' ', input.indexOf("msg")));
		String jsonForm = "{ " + "\"Msg\" : " + "\"" + msg + "\", " + "\"To\" : " + "\"" + to + "\", " + "\"From\" : " + "\"" + from + "\"" + " }";

		return jsonForm;  
	}
}