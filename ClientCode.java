import java.io.*;
import java.net.*;

public class ClientCode {
	public static void main(String[] args) {
		try {
			Socket s = new Socket("127.0.0.1", 8765);
			BufferedReader readStream = new BufferedReader(new InputStreamReader(s.getInputStream()));
			PrintWriter writeStream  = new PrintWriter(s.getOutputStream(), true);
			System.out.println(readStream.readLine());
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			String username = br.readLine();
			writeStream.println(username);
			ClientRead cr = new ClientRead(s);
			ClientWrite cw = new ClientWrite(s, username);
			cr.start();
			cw.start();
		} catch(IOException e) {
			System.out.println(e);
		}
	}
}