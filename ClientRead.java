import java.io.*;
import java.net.*;
import java.net.Socket;

class ClientRead extends Thread {
	Socket client;
	ClientRead(Socket client) {
		this.client = client;
	}

	public void run() {
		String response = "";
		try {
			BufferedReader serverRead = new BufferedReader(new InputStreamReader(client.getInputStream()));
			while(!client.isInputShutdown()) {
				// System.out.println("Inside ClientRead");
				response = serverRead.readLine();
				if(response != null)
					System.out.println(response);
			}
		} catch(IOException e) {
			System.exit(1);
		}
	}
}