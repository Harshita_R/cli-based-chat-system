import java.io.*;
import java.net.*;

public class ServerCode {
	public static void main(String[] args) throws IOException {
		ServerSocket s = new ServerSocket(8765);
		ClientRecords records = new ClientRecords();

		Socket clientSocket;
		String clientUsername;
		try {
			while(true) {
				clientSocket = s.accept();
				PrintWriter writeStream = new PrintWriter(clientSocket.getOutputStream(), true);
				writeStream.println("Enter Your Username: ");
				BufferedReader readStream = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
				clientUsername = readStream.readLine();
				records.addClient(clientUsername, clientSocket);
				// records.printHashMap();
				ServerRead sr = new ServerRead(clientSocket, records, clientUsername);
				sr.start();
			}	
		} catch(IOException e) {
			System.out.println(e);
		}
	}
}