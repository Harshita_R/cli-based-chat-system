import java.io.*;
import java.net.*;
import java.net.Socket;
import java.util.Map;

class ServerRead extends Thread {
	Socket clientSocket;
	ClientRecords records;
	String clientUsername;
	Map<String, Socket> clients;

	ServerRead(Socket clientSocket, ClientRecords records, String clientUsername) {
		this.clientSocket = clientSocket;
		this.records = records;
		this.clientUsername = clientUsername;
	}

	public void run() {
		try {
			BufferedReader readStream = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
			String clientMessage = "";

			while(clientMessage!= null && !"exit".equalsIgnoreCase(clientMessage)) {
				clientMessage = readStream.readLine();
				//clientMessage would hold null if the outputStream has been closed by the cient
				if(clientMessage!= null && !"exit".equalsIgnoreCase(clientMessage)) {
					ServerWrite sw = new ServerWrite(clientMessage, records);
					sw.start();
				} else if("exit".equalsIgnoreCase(clientMessage)) {
					records.removeClient(clientUsername);
					break;
				}
			}
			readStream.close();
			clientSocket.close();
		} catch(IOException e) {
			e.printStackTrace();
		}
	}
}