import java.util.Map;
import java.io.*;
import java.net.*;

class ServerWrite extends Thread {
    String message;
    JsonParser parser;  
    ClientRecords records;
    Map<String, Socket> clients;
    
    public ServerWrite(String clientMessage, ClientRecords records) {
        parser = new JsonParser(clientMessage);
        this.records = records;
    }

    public void run() {
        try {
            String to = parser.getReceiverUsername();
            String from = parser.getSenderUsername();
            message = parser.getMessage();
            // System.out.println("Hashmap");
            // records.printHashMap();
            clients = records.getClients();
            Socket receiverSocket = clients.get(to);
            Socket senderSocket = clients.get(from);
            if(receiverSocket != null && (!receiverSocket.isClosed())) {
                PrintWriter writeStream = new PrintWriter(receiverSocket.getOutputStream(), true);
                writeStream.println("From: " + from + ": " + message);            
            } else {
                PrintWriter writeStream = new PrintWriter(senderSocket.getOutputStream(), true);
                writeStream.println("User is unavailable, try after some time");
            }
        } catch(IOException e) {
            System.out.println(e);
        }
    }
}
