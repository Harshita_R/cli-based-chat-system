import java.io.*;
import java.net.*;

class ClientWrite extends Thread {
	Socket client;
	String myUsername;
	JsonFormatter inputFormatter;
	ClientWrite(Socket client, String username) {
		this.client = client;
		this.myUsername = username;
		this.inputFormatter = new JsonFormatter();
	}

	public void run() {
		try {
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			PrintWriter serverWrite = new PrintWriter(client.getOutputStream(), true);
			
			String input = "";
			while(!"exit".equalsIgnoreCase(input)) {
				// System.out.println("Inside ClientWrite");
				input = br.readLine();
				if(!"exit".equalsIgnoreCase(input)) {
					String jsonInput = inputFormatter.formatInput(input, myUsername);
					serverWrite.println(jsonInput);
				} else {
					serverWrite.println("exit");
				}
			}
			br.close();
			client.shutdownInput();
			client.shutdownOutput();
			client.close();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
}