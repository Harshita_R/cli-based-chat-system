# CLI based chat system using Socket Programming

### I have created a simple command-line-based chat system using Java's socket programming in a multi-threaded environment. The chat system will consist of a server and multiple clients that can communicate with each other.

## Prerequisite
- JDK (Java Development Kit) on your system.
  
## Steps to run the program
1. Set up the server i.e make it halt to recieve or accept any client trying to connect to the server for using it to chat with another clients. (Run the command, on one terminal, "java ServerCode") it internally starts the ServerRead thread where server starrts listening.
2. Create a client by creating a client socket give it a username (Run the command "java ClientCode" for multiple clients run this command on different terminals) it internally starts the ClientRead and ClientWrite Threads.
3. Now You can create multiple clients and chat with other clients via server.

## About the system
- Message format should be as follows
  - "Username msg {type anything as a message}"
- Message is sent in Json format, to parse it a custom JsonParser is made.
- To exit the chat or system just type "exit"