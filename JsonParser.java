class JsonParser {
	String message;
	public JsonParser(String clientMessage) {
		this.message = clientMessage;
	}

	public String getReceiverUsername() {
		int startOfTo = this.message.indexOf("\"", this.message.indexOf(":", this.message.indexOf("\"To\""))) + 1;
        int endOfTo = this.message.indexOf("\"", startOfTo);
        return this.message.substring(startOfTo, endOfTo);      
	}

	public String getMessage() {
		int startOfMsg = this.message.indexOf("\"", this.message.indexOf(":", this.message.indexOf("\"Msg\""))) + 1;
        int endOfMsg = this.message.indexOf("\"", startOfMsg);
        return this.message.substring(startOfMsg, endOfMsg);
	}

	public String getSenderUsername() {
		int startOfFrom = this.message.indexOf("\"", this.message.indexOf(":", this.message.indexOf("\"From\""))) + 1;
        int endOfFrom = this.message.indexOf("\"", startOfFrom); 
        return this.message.substring(startOfFrom, endOfFrom);
	}
}